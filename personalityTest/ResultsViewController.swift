//
//  ResultsViewController.swift
//  personalityTest
//
//  Created by Detravious Brinkley on 2/21/20.
//  Copyright © 2020 Detravious Brinkley. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    
    @IBOutlet weak var resultAnswerLabe: UILabel!
    
    @IBOutlet weak var resultDefinitionLabe: UILabel!
    
    //! because it could be nil when initialized
    var responses: [answer]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        calcPersonallityResults()
    }
    
    func calcPersonallityResults(){
        var frequencyOfAnswers: [animalType: Int] = [:]
        let responseTypes = responses.map{ $0.type }
        
        for response in responseTypes{
            frequencyOfAnswers[response] = (frequencyOfAnswers[response] ?? 0) + 1}
      let frequentAnswersSorted = frequencyOfAnswers.sorted(by:
        { (pair1, pair2) -> Bool in
            return pair1.value > pair2.value
        })
        let mostCommonAnswer = frequentAnswersSorted.first!.key
        
        resultAnswerLabe.text = "You are a \(mostCommonAnswer.rawValue)!"
        
        resultDefinitionLabe.text = mostCommonAnswer.definition

    }
}

