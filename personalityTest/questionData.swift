//
//  questionData.swift
//  personalityTest
//
//  Created by Detravious Brinkley on 2/23/20.
//  Copyright © 2020 Detravious Brinkley. All rights reserved.
//

import Foundation

struct question{
    var text: String
    var type: responseType
    var answers: [answer]
}

enum responseType{
    case single, multiple, ranged
}

struct answer{
    var text: String
    var type: animalType
}

enum animalType: Character{
    case dog = "🐶", cat = "🐱", rabbit = "🐰" , turtle = "🐢"
    
    var definition: String{
        switch self {
        case .dog:
            return "You are outgoing. You surround yourslf with the people you love and enjoy activities with your friends."
        case .cat:
            return "Mischievous yet mild-tempered, you enjoy doing things on your own"
        case .rabbit:
            return "You love everything that's soft. You are healthy and full of energy"
        case .turtle:
            return "You are wise beyound your years and you focus on the details. Slow and stead wins the race."
        }
    }
}
