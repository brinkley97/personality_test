//
//  QuestionViewController.swift
//  personalityTest
//
//  Created by Detravious Brinkley on 2/21/20.
//  Copyright © 2020 Detravious Brinkley. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {
//how things lay out visually
    
    @IBOutlet weak var questionLabel: UILabel!
    
    
    @IBOutlet weak var singleStackView: UIStackView!
    @IBOutlet weak var singleButton1: UIButton!
    @IBOutlet weak var singleButton2: UIButton!
    @IBOutlet weak var singleButton3: UIButton!
    @IBOutlet weak var singleButton4: UIButton!
    
    @IBOutlet weak var multipleStackView: UIStackView!
    @IBOutlet weak var multiLabel1: UILabel!
    @IBOutlet weak var multiLabel2: UILabel!
    @IBOutlet weak var multiLabel3: UILabel!
    @IBOutlet weak var multiLabel4: UILabel!
        
    //need to know the state
    @IBOutlet weak var multiSwitch1: UISwitch!
    @IBOutlet weak var multiSwitch2: UISwitch!
    @IBOutlet weak var multiSwitch3: UISwitch!
    @IBOutlet weak var multiSwitch4: UISwitch!
    
    @IBOutlet weak var rangedStackView: UIStackView!
    @IBOutlet weak var rangedLabel1: UILabel!
    @IBOutlet weak var rangedLabel2: UILabel!
    
    @IBOutlet weak var rangedSlider: UISlider!
    
    @IBOutlet weak var questionProgressView: UIProgressView!
    
    
    
    
    
    var questions: [question] = [
        
        // 1st object in array
        question(text: "Which food do you like the most?",
                 type: .single,
                 answers: [
                    answer(text: "Steak", type: .dog),
                    answer(text: "Fish", type: .cat),
                    answer(text: "Carrots", type: .rabbit),
                    answer(text: "Corn", type: .turtle)
        ]),
        question(text: "Which activities do you enjoy?",
                 type: .multiple,
                 answers: [
                    answer(text: "Eating", type: .dog),
                    answer(text: "Sleeping", type: .cat),
                    answer(text: "Cuddling", type: .rabbit),
                    answer(text: "Swimming", type: .turtle)
        ]),
        question(text: "How much do you enjoy car rides?",
                 type: .ranged,
                 answers: [
                    answer(text: "I love them.", type: .dog),
                    answer(text: "I dislike them.", type: .cat),
                    answer(text: "I get a little nervous.", type: .rabbit),
                    answer(text: "I barely notice them.", type: .turtle)
        ]),
    ]
    
    //go from one question to the next- increment to know which question to show
    var questionIndex = 0
    //record the answer they have chosen
    var answersChosen: [answer] = []
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        updateUI()
    }
    
    func updateUI(){
        
        //hide everything
        singleStackView.isHidden = true
        multipleStackView.isHidden = true
        rangedStackView.isHidden = true
        
        //+1 to get question 1 from array
        navigationItem.title = "Question #\(questionIndex+1)"
        let currentQuestion = questions[questionIndex]
        let currentAnswers = currentQuestion.answers
        let totalProgress = Float(questionIndex) / Float(questions.count)
        
        //update question label and progress view
        questionLabel.text = currentQuestion.text
        questionProgressView.setProgress(totalProgress, animated: true)
        
        switch currentQuestion.type {
        case .single:
            updateSingleStack(using: currentAnswers)
        case .multiple:
            updateMultipleStack(using: currentAnswers)
        case .ranged:
            updateRangedStack(using: currentAnswers)
        }
    }
    
    func updateSingleStack(using answers: [answer]){
        singleStackView.isHidden = false
        //.normal = when the button is just sitting there show the following text
        singleButton1.setTitle(answers[0].text, for: .normal)
        singleButton2.setTitle(answers[1].text, for: .normal)
        singleButton3.setTitle(answers[2].text, for: .normal)
        singleButton4.setTitle(answers[3].text, for: .normal)

    }
    func updateMultipleStack(using answers: [answer]){
        multipleStackView.isHidden = false
        multiSwitch1.isOn = false
        multiSwitch2.isOn = false
        multiSwitch3.isOn = false
        multiSwitch4.isOn = false
        
        multiLabel1.text = answers[0].text
        multiLabel2.text = answers[1].text
        multiLabel3.text = answers[2].text
        multiLabel4.text = answers[3].text
    }
    func updateRangedStack(using answers: [answer]){
        rangedStackView.isHidden = false
        
        //set in middle as a default
        rangedSlider.setValue(0.5, animated: false)
        
        rangedLabel1.text = answers.first?.text
        rangedLabel2.text = answers.last?.text
    }
    //when answer is selected
    @IBAction func singleAnsButPressed(_ sender: UIButton) {
        let currentAnswers = questions[questionIndex].answers
        
        switch sender{
        case singleButton1:
            answersChosen.append(currentAnswers[0])
        case singleButton2:
            answersChosen.append(currentAnswers[1])
        case singleButton3:
            answersChosen.append(currentAnswers[2])
        case singleButton4:
            answersChosen.append(currentAnswers[3])
        default:
            break
        }
        
        nextQuestion()
    }
    
    @IBAction func multiAnsButPressed(){
         let currentAnswers = questions[questionIndex].answers
        
        if multiSwitch1.isOn{
            answersChosen.append(currentAnswers[0])
        }
        if multiSwitch2.isOn{
            answersChosen.append(currentAnswers[1])
        }
        if multiSwitch3.isOn{
            answersChosen.append(currentAnswers[2])
        }
        if multiSwitch4.isOn{
            answersChosen.append(currentAnswers[3])
        }
        nextQuestion()
    }
    
    
    @IBAction func rangedAnsButPressed() {
        let currentAnswers = questions[questionIndex].answers
        let index = Int(round(rangedSlider.value * Float(currentAnswers.count - 1)))
        //count - 1 gives us the 3rd index in the array... why? we dont have a 4th index
        answersChosen.append(currentAnswers[index])
        
        nextQuestion()
    }
    
    
    func nextQuestion(){
        questionIndex += 1
        
        if questionIndex < questions.count{
            updateUI()
        } else{
            //Takes us to the end screen
            performSegue(withIdentifier: "ResultsSegue", sender: nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //I want to do something before calling the segue
        if segue.identifier == "ResultsSegue" {
//            save and post answers
            let resultsViewController = segue.destination as! ResultsViewController
            resultsViewController.responses = answersChosen
        }
    }
    
}
